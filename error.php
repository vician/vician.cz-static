<?php
// number of error
$number = -1;
// texts for number of error
$errors = array(
    -1  => "",
    404 => "Page not found",
);

// error from get
if(isset($_GET['e']) && isset($errors[$_GET['e']])) {
    $number = $_GET['e'];
}

$text = $errors[$number];

$head = "vician.cz";
if($number != -1) {
    $head .= " - ".$text;
    $title = "Error ".$number;
    
} else {
    $title = "Unknow error";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $head; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            html {
                margin: auto auto;
                padding: auto auto;
                display: table;
            }

            body {
                background: black;
                padding: auto auto;
                display: table-cell;
                vertical-align: middle;
                padding-top: 25%;
            }
            
            h1 {
                color: darkred;
                font-weight: bold;
                text-align: center;
                font-size: 5em;
            }
            
            .sorry {
                text-align: center;
                font-style: italic;
                color: gray;
            }
            
            .text {
                text-align: center;
                color: darkred;
                font-weight: bold;
                font-size: 2em;
            }
            
            .copy {
                text-align: right;
                color: gray;
            }
            
            .copy a {
                color: gray;
                text-decoration: none;
            }
            
            .copy a:hover {
                color: white;
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <h1><?php echo $title; ?></h1>
        <div class="sorry">I'm sorry</div>
        <div class="text"><?php echo $text; ?></div>
        <div class="copy"><a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/">&copy; Vicián</a></div>
    </body>
</html>
